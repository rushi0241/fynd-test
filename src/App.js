import React from 'react';
import { Container, Row, Col, Image, Modal, Button } from 'react-bootstrap';
import editIcon from './images/edit.png';
import imgNotFound from './images/notfound.png';
import data from './services/data'
import './App.css';

const imgData = data;
console.log(imgData);;

function App() {   
  const [modalShow, setModalShow] = React.useState(false)
  return (
    <div className="App">
      <Container fluid={true}>
        <Row>
          {imgData.map((card, index) => {
            return <Col sm={3} key={index}>
              <div className="card-img">
                <Image src={card.image_url} alt={card.name} title={card.name} fluid />
                <div class="img-overley">
                  <a href="javascript:;" onClick={(showImg) => setModalShow(true, index)}>
                    <Image src={editIcon} alt="Edit" title="Edit"/>
                    <p>Edit</p>
                  </a>
                </div> 
              </div>
              <p>{card.name}</p>
            </Col>
          })}
        </Row>
      </Container>
      
      <MyVerticallyCenteredModal
      show={modalShow}
      onHide={() => setModalShow(false)}
    />
    </div>
  );
}

export default App;

function MyVerticallyCenteredModal(props) {
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Edit Image
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Row>
          <Col sm={6}>
            <p>Orignal Image</p>
            <Image src="" fluid/>
          </Col>
          <Col sm={6}>
            <p>New Image</p>
            <Image src={imgNotFound} fluid alt="Image not found" title="Image not found"/>
            <input type="file" name="myFile"/>
          </Col>
        </Row>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={props.onHide}>Close</Button>
      </Modal.Footer>
    </Modal>
  );
}
